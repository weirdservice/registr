#!/usr/bin/env node
import program from 'commander'
import NRP from 'node-redis-pubsub'
import crypto from 'crypto'
import mongoose from 'mongoose'
import {spawn} from 'child_process'

import pkg from '../package.json'

program
  .version(pkg.version)
  .usage('<configurations..>')
  .parse(process.argv)

const config = { port: 6379, scope: 'test' },
      nrp = new NRP(config)

const key = crypto.randomBytes(32).toString('hex')

// db
mongoose.connect('mongodb://localhost/test')
var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function (callback) {
  // do some pubsub
  console.log('DB Connected')
})

const siteSchema = mongoose.Schema({
    site: String,
    feedUrl: String,
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now },
    lastScrapped: { type: Date, default: Date.now }
})

const Sites = mongoose.model('Sites', siteSchema)

var mock = {}
var requested = []

const exists = (data) => {
  Sites.find({site: data.url}, (err, sites) => {
    if (err)
      return console.error(err)

    if (sites.length == 0) {
      nrp.emit('registr:insert', {site: data.url})
    }
  })
}

const insert = (data) => {
  console.log('insert', data.site)

  data.feedUrl = run('findr', [data.site], (result) => {
    console.log('result:', result, 'end')

    let siteResult = JSON.parse(result)

    if(siteResult.status == 'success' && siteResult.feeds.length > 0) {
      let newSite = new Sites({
        site: siteResult.url,
        feedUrl: siteResult.feeds[0].href,
        created: Date.now(),
        updated: Date.now(),
        lastScrapped: Date.now()
      })
      newSite.save( (err, site) => {
        if (err) return console.error(err)
        console.log(site)
      })
    }
  })
}

const run = (cmd, options, callback) => {
    let command = spawn(cmd, options)
    command.stdout.setEncoding('utf8')

    var result = ''
    command.stdout.on('data', (data) => {
         result += data.toString()
    })

    command.on('close', (code) => {
        return callback(result)
    })
}

const register = () => {
  nrp.emit('castr:add:registr', {id: key})
}

nrp.on('registr:exists:' + key, exists)
nrp.on('registr:insert:' + key, insert)

nrp.on('castr:callback', register)

process.on('SIGINT', () => {
    console.log("\nBye!")
    nrp.emit('castr:remove:registr', {id: key})
    nrp.quit()
    process.exit()
})

register()
console.log('Listen registr:* as ' + key)
