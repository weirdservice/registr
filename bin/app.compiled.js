#!/usr/bin/env node
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _nodeRedisPubsub = require('node-redis-pubsub');

var _nodeRedisPubsub2 = _interopRequireDefault(_nodeRedisPubsub);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _child_process = require('child_process');

var _packageJson = require('../package.json');

var _packageJson2 = _interopRequireDefault(_packageJson);

_commander2['default'].version(_packageJson2['default'].version).usage('<configurations..>').parse(process.argv);

var config = { port: 6379, scope: 'test' },
    nrp = new _nodeRedisPubsub2['default'](config);

var key = _crypto2['default'].randomBytes(32).toString('hex');

// db
_mongoose2['default'].connect('mongodb://localhost/test');
var db = _mongoose2['default'].connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  // do some pubsub
  console.log('DB Connected');
});

var siteSchema = _mongoose2['default'].Schema({
  site: String,
  feedUrl: String,
  created: { type: Date, 'default': Date.now },
  updated: { type: Date, 'default': Date.now },
  lastScrapped: { type: Date, 'default': Date.now }
});

var Sites = _mongoose2['default'].model('Sites', siteSchema);

var mock = {};
var requested = [];

var exists = function exists(data) {
  Sites.find({ site: data.url }, function (err, sites) {
    if (err) return console.error(err);

    if (sites.length == 0) {
      nrp.emit('registr:insert', { site: data.url });
    }
  });
};

var insert = function insert(data) {
  console.log('insert', data.site);

  data.feedUrl = run('findr', [data.site], function (result) {
    console.log('result:', result, 'end');

    var siteResult = JSON.parse(result);

    if (siteResult.status == 'success' && siteResult.feeds.length > 0) {
      var newSite = new Sites({
        site: siteResult.url,
        feedUrl: siteResult.feeds[0].href,
        created: Date.now(),
        updated: Date.now(),
        lastScrapped: Date.now()
      });
      newSite.save(function (err, site) {
        if (err) return console.error(err);
        console.log(site);
      });
    }
  });
};

var run = function run(cmd, options, callback) {
  var command = (0, _child_process.spawn)(cmd, options);
  command.stdout.setEncoding('utf8');

  var result = '';
  command.stdout.on('data', function (data) {
    result += data.toString();
  });

  command.on('close', function (code) {
    return callback(result);
  });
};

var register = function register() {
  nrp.emit('castr:add:registr', { id: key });
};

nrp.on('registr:exists:' + key, exists);
nrp.on('registr:insert:' + key, insert);

nrp.on('castr:callback', register);

process.on('SIGINT', function () {
  console.log("\nBye!");
  nrp.emit('castr:remove:registr', { id: key });
  nrp.quit();
  process.exit();
});

register();
console.log('Listen registr:* as ' + key);
